
export const INCREMENT = "INCREMENT";
export const DECREMENT = "DECREMENT";
export const RESET = "RESET";

export function incrementAction() {
    return { type: INCREMENT }
}

export function decrementAction() {
    return { type: DECREMENT }
}

export function resetAction() {
    return { type: RESET }
}

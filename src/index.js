import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, createStore} from "redux";
import Counter from "./features/counter/Counter";
import {Provider} from "react-redux";
import {DECREMENT, decrementAction, INCREMENT, incrementAction, RESET, resetAction} from "./actions";
import thunk from "redux-thunk";

const initialState = {
    count: 0
};

function reducer(state = initialState, action) {
    console.log('reducer', state, action);

    switch (action.type) {
        case INCREMENT:
            return {
                count: state.count + 1
            };
        case DECREMENT:
            return {
                count: state.count - 1
            };
        case RESET:
            return {
                count: 0
            };
        default:
            return state;
    }
}

const store = createStore(
    reducer,
    applyMiddleware(thunk)
);

const App = () => (
    <div>
        <Counter />
    </div>
);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

import React from 'react';
import {connect} from "react-redux";
import {decrementAction, incrementAction} from "../../actions";

class Counter extends React.Component {

    interval;

    constructor(props) {
        super(props);
    }

    increment = () => {
        this.props.increment();
    }

    decrement = () => {
        this.props.decrement();
    }

    createInterval = () => {
        this.interval = setInterval(function (increment) {
            increment();
        }, 1000, this.props.increment);

        console.log('Created interval: ' + this.interval);
    }

    componentDidMount() {
        this.createInterval();
    }

    stop = () => {
        console.log('Stopping timer');
        console.log(this.interval);
        clearInterval(this.interval);
    }

    play = () => {
        this.createInterval();
    }

    render() {
        return (
            <div>
                <h2>Counter</h2>
                <div>
                    <button onClick={this.decrement}>-</button>
                    <span>{this.props.count}</span>
                    <button onClick={this.increment}>+</button>

                </div>

                <div>
                    <button onClick={this.stop}> STOP </button>
                    <button onClick={this.play}> PLAY </button>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        count: state.count
    };
}

const mapDispatchToProps = {
    increment: incrementAction,
    decrement: decrementAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
